from Tile import * 

class Singleton(type):
    _instances = {}
    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        else:
            cls._instances[cls].__init__(*args, **kwargs)
        return cls._instances[cls]

class Board(metaclass=Singleton):
    def __init__(self, y_axis, x_axis):
        self.y_axis = y_axis
        self.x_axis = x_axis
        self.board = [[Tile() for i in range(self.y_axis)] for j in range(self.x_axis)]



    def printBoard(self):
        HIDDEN = 'H'
        WALL = 'X'
        BOMB = '*'
        print (self.board)